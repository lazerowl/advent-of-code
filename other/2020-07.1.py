#!/usr/bin/python3

from pprint import pprint

TEST_INPUT = '''light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.'''

def make_bags_dict():
    bags = {}

    for line in TEST_INPUT.splitlines():
        # Split on separator string 'bags contain', strip whitespace
        (color, contents) = (s.strip() for s in line.split('bags contain'))

        # Remove fluff
        contents = (s
                .replace('no other', '')
                .replace('bags', '')
                .replace('bag', '')
                .replace('.', '')
                .strip() for s in contents.split(','))

        # Strip count from contents (first field separated by whitespace), it's not meaningful for answer 1
        contents = list(' '.join(s.split(' ')[1:]) for s in contents)

        # 'color' is now the color definition string of the current bag.
        # 'contents' is now a list of strings, each relating to another color definition.
        # Caveat: if the list contains a single '' (byproduct of fluff removal) string, set the value to None instead.
        bags[color] = contents if contents[0] else None

    return bags

def search_color(bags, current_bag, target):
    '''
    Recursively search the contents in <current_bag> for <target>.
    Return True if <target> is found, otherwise return False.
    '''

    if bags[current_bag] == None:
        # There are no bags in this bag, so we won't find it here.
        return False

    if target in bags[current_bag]:
        # The target is in this bag!
        return True

    # <current_bag> contains a list of other bags.
    # Perfom lazy recursive search for <target>
    for bag in bags[current_bag]:
        if search_color(bags, bag, target):
            return True

    # None of the bags in <contents> contained the <target>
    return False

if __name__ == '__main__':
    bags = make_bags_dict()

    count = 0
    for bag in bags.keys():
        if search_color(bags, bag, 'shiny gold'):
            count += 1
    assert count == 4

    pprint(bags)
