use std::fs::read;

fn main() {
    let input = read("input/2020-day05.input").unwrap();
    let input = String::from_utf8_lossy(&input);
    let lines = input.trim().split("\n");

    let mut ids: Vec<u16> = lines.map(|line| parse(line)).collect();
    ids.sort();

    println!("Highest seat ID: {}", ids.last().unwrap());

    for i in &ids {
        if !ids.contains(&(i + 1)) {
            println!("Missing seat ID: {}", i + 1);
            break;
        }
    }
}

fn parse(spec: &str) -> u16 {
    let mut id = 0u16;

    for b in spec.chars() {
        id <<= 1;
        id |= match b {
            'F' | 'L' => 0,
            'B' | 'R' => 1,
            _ => panic!("Invalid character: {}", b),
        }
    }

    id
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test() {
        let cases = [
            ("FBFBBFFRLR", 357u16),
            ("BFFFBBFRRR", 567u16),
            ("FFFBBBFRRR", 119u16),
            ("BBFFBBFRLL", 820u16),
        ];

        for case in cases.iter() {
            assert_eq!(parse(case.0), case.1);
        }
    }
}
