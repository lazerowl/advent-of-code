fn main() {
    use std::fs::read;
    let input = String::from_utf8_lossy(
        &read("input/2020-day12.input").expect("Failed to read input file"),
    )
    .to_string();

    let actions: Vec<Action> = input.lines().map(|line| Action::from_str(line)).collect();
    let mut ship = Ship::new();
    for action in actions {
        ship.move_ship(&action);
    }

    println!(
        "Answer A: {} + {} = {}",
        ship.pos.x.abs(),
        ship.pos.y.abs(),
        ship.pos.x.abs() + ship.pos.y.abs()
    );

    let actions: Vec<Action> = input.lines().map(|line| Action::from_str(line)).collect();
    let mut ship = Ship::new();
    for action in actions {
        ship.move_waypoint(&action);
    }
    println!(
        "Answer B: {} + {} = {}",
        ship.pos.x.abs(),
        ship.pos.y.abs(),
        ship.pos.x.abs() + ship.pos.y.abs()
    );
}

#[derive(Debug)]
enum Action {
    North(isize),
    East(isize),
    South(isize),
    West(isize),
    Left(isize),
    Right(isize),
    Forward(isize),
}

impl Action {
    fn from_str(s: &str) -> Self {
        let action = s.chars().next().expect("Input string was empty");
        let value = s[1..].parse().expect("Malformed input");

        use Action::*;
        match &action.to_uppercase().to_string()[..] {
            "N" => North(value),
            "E" => East(value),
            "S" => South(value),
            "W" => West(value),
            "L" => Left(value),
            "R" => Right(value),
            "F" => Forward(value),
            _ => panic!("Invalid action char"),
        }
    }
}

enum Facing {
    North,
    East,
    South,
    West,
}

impl Facing {
    fn from_bearing(mut bearing: isize) -> Self {
        if bearing < 0 {
            bearing += 360;
        } else if bearing >= 360 {
            bearing -= 360;
        }

        use Facing::*;
        match bearing {
            0 => North,
            90 => East,
            180 => South,
            270 => West,
            _ => panic!("Invalid bearing: {}", bearing),
        }
    }

    fn to_bearing(&self) -> isize {
        use Facing::*;
        match self {
            North => 0,
            East => 90,
            South => 180,
            West => 270,
        }
    }

    fn to_action(&self, value: &isize) -> Action {
        use Facing::*;
        match self {
            North => Action::North(*value),
            East => Action::East(*value),
            South => Action::South(*value),
            West => Action::West(*value),
        }
    }
}

#[derive(Debug)]
struct Point {
    x: isize,
    y: isize,
}

impl Point {
    fn rotate(&mut self, mut degrees: isize) {
        assert!(degrees % 90 == 0);

        while degrees < 0 {
            degrees += 360;
        }

        while degrees > 0 {
            let tmp = self.x;
            self.x = self.y;
            self.y = -tmp;
            degrees -= 90;
        }
    }
}

struct Ship {
    pos: Point,
    facing: Facing,
    waypoint: Point,
}

impl Ship {
    fn new() -> Self {
        use Facing::*;
        Ship {
            pos: Point { x: 0, y: 0 },
            facing: East,
            waypoint: Point { x: 10, y: 1 },
        }
    }

    fn move_ship(&mut self, action: &Action) {
        use Action::*;
        match action {
            North(value) => self.pos.y += value,
            East(value) => self.pos.x += value,
            South(value) => self.pos.y -= value,
            West(value) => self.pos.x -= value,
            Left(value) => {
                self.facing = Facing::from_bearing(self.facing.to_bearing() - value);
            }
            Right(value) => {
                self.facing = Facing::from_bearing(self.facing.to_bearing() + value);
            }
            Forward(value) => self.move_ship(&self.facing.to_action(&value)),
        }
    }

    fn move_towards_waypoint(&mut self, times: isize) {
        self.pos.x += self.waypoint.x * times;
        self.pos.y += self.waypoint.y * times;
    }

    fn move_waypoint(&mut self, action: &Action) {
        use Action::*;
        match action {
            North(value) => self.waypoint.y += value,
            East(value) => self.waypoint.x += value,
            South(value) => self.waypoint.y -= value,
            West(value) => self.waypoint.x -= value,
            Left(value) => self.waypoint.rotate(-*value),
            Right(value) => self.waypoint.rotate(*value),
            Forward(value) => self.move_towards_waypoint(*value),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_2020_12a() {
        let mut ship = Ship::new();
        for s in ("F10,N3,F7,R90,F11").split(',') {
            ship.move_ship(&Action::from_str(s));
        }

        assert_eq!((17, 8), (ship.pos.x.abs(), ship.pos.y.abs()));
    }

    #[test]
    fn test_2020_12b() {
        let mut ship = Ship::new();
        for s in ("F10,N3,F7,R90,F11").split(',') {
            ship.move_waypoint(&Action::from_str(s));
        }

        assert_eq!((214, 72), (ship.pos.x.abs(), ship.pos.y.abs()));
    }

    #[test]
    fn test_rotate() {
        let mut point = Point { x: 2, y: 1 };
        point.rotate(90);
        assert_eq!((1, -2), (point.x, point.y));

        point.rotate(-90);
        assert_eq!((2, 1), (point.x, point.y));

        point.rotate(270);
        assert_eq!((-1, 2), (point.x, point.y));

        point.rotate(-180);
        assert_eq!((1, -2), (point.x, point.y));
    }
}
