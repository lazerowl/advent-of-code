use std::{
    collections::{HashMap, HashSet},
    fs::read,
};

fn main() {
    let program = String::from_utf8_lossy(&read("input/2020-day14.input").unwrap()).to_string();
    println!("Answer A: {}", answer_a(&program));
    println!("Answer B: {}", answer_b(&program));
}

fn answer_a(program: &str) -> u64 {
    let mut memory: HashMap<usize, u64> = HashMap::new();
    let mut mask = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";

    for line in program.lines() {
        let mut tokens = line.split('=').map(|s| s.trim());

        let op = tokens.next().unwrap();
        let address: usize;
        let value: u64;

        if &op[..] == "mask" {
            mask = tokens.next().unwrap();
        } else if op.starts_with("mem") {
            address = op
                .split_terminator(&['[', ']'][..])
                .nth(1)
                .unwrap()
                .parse()
                .unwrap();
            value = tokens.next().unwrap().parse().unwrap();

            let mut masked = 0u64;
            for (n, m) in mask.chars().rev().enumerate() {
                masked |= match m {
                    '0' => 0,
                    '1' => (1 << n),
                    'X' => (value & (1 << n)),
                    _ => panic!("Invalid value"),
                };
            }

            memory.insert(address, masked);
        }
    }

    memory.values().sum::<u64>()
}

fn answer_b(program: &str) -> u64 {
    let mut memory: HashMap<usize, u64> = HashMap::new();
    let mut mask = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";

    for line in program.lines() {
        let mut tokens = line.split('=').map(|s| s.trim());

        let op = tokens.next().unwrap();
        let value: u64;

        if &op[..] == "mask" {
            mask = tokens.next().unwrap();
        } else if op.starts_with("mem") {
            let base_address: usize = op
                .split_terminator(&['[', ']'][..])
                .nth(1)
                .unwrap()
                .parse()
                .unwrap();
            value = tokens.next().unwrap().parse().unwrap();
            let base_address = mask_address(base_address, mask);

            let mut addresses = HashSet::new();
            step_mask(mask, 0, base_address, &mut addresses);

            for addr in addresses {
                memory.insert(addr, value);
            }
        }
    }

    memory.values().sum::<u64>()
}

fn mask_address(mut address: usize, mask: &str) -> usize {
    for (n, c) in mask.chars().rev().enumerate() {
        match c {
            '0' => (),
            '1' => address |= 1 << n,
            'X' => (),
            _ => panic!("Invalid mask char"),
        }
    }

    address
}

fn step_mask(mask: &str, bitpos: usize, address: usize, acc: &mut HashSet<usize>) {
    acc.insert(address);

    if bitpos >= mask.len() {
        return;
    }

    match mask
        .chars()
        .rev()
        .nth(bitpos)
        .expect("Error accessing mask bit")
    {
        'X' => {
            step_mask(mask, bitpos + 1, address | 1 << bitpos, acc);
            step_mask(mask, bitpos + 1, address & !(1 << bitpos), acc);
        }
        _ => step_mask(mask, bitpos + 1, address, acc),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_2020_14a() {
        let mut program = String::new();
        program.push_str("mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X\n");
        program.push_str("mem[8] = 11\n");
        program.push_str("mem[7] = 101\n");
        program.push_str("mem[8] = 0\n");

        assert_eq!(answer_a(&program), 165);
    }

    #[test]
    fn test_mask_address() {
        let mask = "000000000000000000000000000000X1001X";
        assert_eq!(mask_address(0b101010, mask), 0b111010);
    }

    #[test]
    fn test_step_mask() {
        let mask = "000000000000000000000000000000X1001X";
        let mut addresses: HashSet<usize> = HashSet::new();
        step_mask(mask, 0, 0b111010, &mut addresses);
        assert_eq!(
            addresses,
            HashSet::<usize>::from(
                [0b011010, 0b011011, 0b111010, 0b111011]
                    .iter()
                    .cloned()
                    .collect()
            )
        );
    }

    #[test]
    fn test_2020_14b() {
        let mut program = String::new();
        program.push_str("mask = 000000000000000000000000000000X1001X\n");
        program.push_str("mem[42] = 100\n");
        program.push_str("mask = 00000000000000000000000000000000X0XX\n");
        program.push_str("mem[26] = 1\n");

        assert_eq!(answer_b(&program), 208);
    }
}
