#[macro_use]
extern crate lazy_static;
extern crate regex;

use {
    regex::Regex,
    std::{
        fs::File,
        io::{BufRead, BufReader, Error, Lines},
    },
};

fn main() {
    println!("Answer 1: {}", answer1("input/2020-day02.input"));
    println!("Answer 2: {}", answer2("input/2020-day02.input"));
}

fn answer1(path: &str) -> usize {
    let mut valid_passwords = 0;

    for line in read_lines(path).expect(&format!("Can't open '{}'", path)) {
        let (min, max, letter, password): (usize, usize, char, String) = parse_line(&line.unwrap());
        let letter_count = password.chars().filter(|&c| c == letter).count();

        if min <= letter_count && max >= letter_count {
            valid_passwords += 1
        };
    }

    valid_passwords
}

fn answer2(path: &str) -> usize {
    let mut valid_passwords = 0;

    for line in read_lines(path).expect(&format!("Can't open '{}'", path)) {
        let (min, max, letter, password): (usize, usize, char, String) = parse_line(&line.unwrap());

        if (password.chars().nth(min - 1).unwrap() == letter)
            ^ (password.chars().nth(max - 1).unwrap() == letter)
        {
            valid_passwords += 1;
        }
    }

    valid_passwords
}

fn parse_line(line: &str) -> (usize, usize, char, String) {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"^(\d+)-(\d+) ([a-z]): ([a-z]+)$").unwrap();
    }

    let captures = RE.captures(line).unwrap();
    (
        captures[1].parse().unwrap(),
        captures[2].parse().unwrap(),
        captures[3].chars().nth(0).unwrap(),
        captures[4].to_string(),
    )
}

fn read_lines(path: &str) -> Result<Lines<BufReader<File>>, Error> {
    Ok(BufReader::new(File::open(path)?).lines())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_line() {
        let input = "4-6 z: abcdzzzzz";
        let (min, max, letter, password): (usize, usize, char, String) = parse_line(input);

        assert_eq!(min, 4);
        assert_eq!(max, 6);
        assert_eq!(letter, 'z');
        assert_eq!(password, "abcdzzzzz");
    }
}
