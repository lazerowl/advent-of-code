use std::{
    fs::File,
    io::{BufRead, BufReader, Error, Lines},
};

fn main() {
    println!("Answer 1: {}", answer1("input/2020-day03.input"));
    println!("Answer 2: {}", answer2("input/2020-day03.input"));
}

fn answer1(path: &str) -> usize {
    let lines = read_lines(path).expect(&format!("Can't open '{}'", path));
    let map = draw_map(lines);
    count_slopes(&map, 3, 1)
}

fn answer2(path: &str) -> usize {
    let lines = read_lines(path).expect(&format!("Can't open '{}'", path));
    let map = draw_map(lines);

    count_slopes(&map, 1, 1)
        * count_slopes(&map, 3, 1)
        * count_slopes(&map, 5, 1)
        * count_slopes(&map, 7, 1)
        * count_slopes(&map, 1, 2)
}

fn draw_map(lines: Lines<BufReader<File>>) -> Vec<Vec<bool>> {
    let mut rows = Vec::<Vec<bool>>::new();

    for line in lines {
        let mut row = Vec::<bool>::new();
        for char in line.unwrap().chars() {
            row.push(match char {
                '.' => false,
                '#' => true,
                _ => panic!("Invalid char: '{}'", char),
            });
        }
        rows.push(row);
    }

    rows
}

fn count_slopes(map: &Vec<Vec<bool>>, right: usize, down: usize) -> usize {
    let mut row = down;
    let mut col = right;
    let mut tree_count = 0;
    let width = map[row].len();

    while row < map.len() {
        match map[row][col % width] {
            true => tree_count += 1,
            false => (),
        }

        row += down;
        col += right;
    }

    tree_count
}

fn read_lines(path: &str) -> Result<Lines<BufReader<File>>, Error> {
    Ok(BufReader::new(File::open(path)?).lines())
}
