use std::{
    fs::File,
    io::{BufRead, BufReader, Error, Lines},
};

const INPUT_PATH: &str = "input/2020-day01.input";

fn main() {
    let input = read_input(INPUT_PATH);

    let (a, b) = find_pair_with_sum(input.clone(), 2020).unwrap();
    println!("Answer 1: {}", a * b);

    let (a, b, c) = find_triplet_with_sum(input, 2020).unwrap();
    println!("Answer 2: {}", a * b * c);
}

fn read_input(path: &str) -> Vec<u32> {
    read_lines(path)
        .expect(&format!("Can't open '{}'", path))
        .map(|line| line.unwrap().parse().unwrap())
        .collect()
}

fn read_lines(path: &str) -> Result<Lines<BufReader<File>>, Error> {
    Ok(BufReader::new(File::open(path)?).lines())
}

fn find_pair_with_sum(mut entries: Vec<u32>, target: u32) -> Option<(u32, u32)> {
    while !entries.is_empty() {
        let entry_a = &entries.pop().unwrap();
        for entry_b in &entries {
            if entry_a + entry_b == target {
                return Some((entry_a.to_owned(), entry_b.to_owned()));
            }
        }
    }

    None
}

fn find_triplet_with_sum(mut entries: Vec<u32>, target: u32) -> Option<(u32, u32, u32)> {
    while !entries.is_empty() {
        let entry_a = entries.pop().unwrap();

        let result: Option<(u32, u32)> = find_pair_with_sum(entries.clone(), target - entry_a);
        if result.is_some() {
            let (entry_b, entry_c) = result.unwrap();
            return Some((entry_a, entry_b, entry_c));
        }
    }

    None
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_find_pair_answer() {
        let input = read_input(INPUT_PATH);
        let (a, b) = find_pair_with_sum(input.clone(), 2020).unwrap();
        assert_eq!(a + b, 2020);
    }

    #[test]
    fn test_find_triplet_answer() {
        let input = read_input(INPUT_PATH);
        let (a, b, c) = find_triplet_with_sum(input.clone(), 2020).unwrap();
        assert_eq!(a + b + c, 2020);
    }
}
