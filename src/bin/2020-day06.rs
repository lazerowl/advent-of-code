use std::{
    collections::{HashMap, HashSet},
    fs::read,
};

fn main() {
    println!("Answer 1: {}", answer1("input/2020-day06.input"));
    println!("Answer 2: {}", answer2("input/2020-day06.input"));
}

fn answer1(path: &str) -> usize {
    let input = read(path).unwrap();
    let input = String::from_utf8_lossy(&input);
    let groups: Vec<&str> = input.trim().split("\n\n").collect();

    let mut count = 0;
    for group in groups {
        let mut unique_answers = HashSet::new();

        for person in group.split('\n').collect::<Vec<&str>>() {
            for answer in person.chars() {
                unique_answers.insert(answer);
            }
        }

        count += unique_answers.len();
    }

    count
}

fn answer2(path: &str) -> usize {
    let input = read(path).unwrap();
    let input = String::from_utf8_lossy(&input);
    let groups: Vec<&str> = input.trim().split("\n\n").collect();

    let mut total_count = 0;
    for group in groups {
        let mut answer_counts = HashMap::<char, usize>::new();

        for person in group.split('\n').collect::<Vec<&str>>() {
            for answer in person.chars() {
                let answer_count = answer_counts.get(&answer);
                match answer_count {
                    Some(&count) => answer_counts.insert(answer, count + 1),
                    None => answer_counts.insert(answer, 1),
                };
            }
        }

        for (_, count) in answer_counts {
            let group_size = group.split('\n').count();
            if count == group_size {
                total_count += 1;
            }
        }
    }

    total_count
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_answer1() {
        assert_eq!(answer1("test-input/2020-day06-test.input"), 11);
    }

    #[test]
    fn test_answer2() {
        assert_eq!(answer2("test-input/2020-day06-test.input"), 6);
    }
}
