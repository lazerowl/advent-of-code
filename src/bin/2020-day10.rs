use std::{
    collections::{BTreeSet, HashMap},
    fs::read,
};

type Jolt = isize;
type JoltSet = BTreeSet<Jolt>;

fn main() {
    let input: JoltSet = String::from_utf8_lossy(
        &read("input/2020-day10.input").expect("Failed to read input file."),
    )
    .split_whitespace()
    .map(|line| line.parse().expect("Bad input."))
    .collect();

    let (a, b) = answer1(&input);
    println!("Ones and threes: {} x {} = {}", a, b, a * b);

    println!("Number of valid permutations: {}", answer2(&input));
}

fn answer1(input: &JoltSet) -> (usize, usize) {
    let mut ones = 0;
    let mut threes = 1;

    let mut last = 0;
    for i in input.iter() {
        match i - last {
            1 => ones += 1,
            3 => threes += 1,
            value => panic!("Invalid joltage difference: {}", value),
        }
        last = *i;
    }

    (ones, threes)
}

type BranchCount = u64;
type JoltMap = HashMap<Jolt, Option<BranchCount>>;

fn answer2(input: &JoltSet) -> u64 {
    let mut joltmap = JoltMap::new();

    joltmap.insert(0, None);

    for value in input.iter() {
        joltmap.insert(*value, None);
    }

    let stop = input.iter().next_back().unwrap() + 3;
    explore_node(&0, &mut joltmap, stop)
}

fn explore_node(jolts: &Jolt, joltmap: &mut JoltMap, stop: Jolt) -> u64 {
    if *jolts == stop {
        return 1;
    }

    if !joltmap.contains_key(jolts) {
        return 0;
    }

    if joltmap[jolts].is_none() {
        let branch_count = explore_node(&(jolts + 1), joltmap, stop)
            + explore_node(&(jolts + 2), joltmap, stop)
            + explore_node(&(jolts + 3), joltmap, stop);
        joltmap.insert(*jolts, Some(branch_count));
    }

    joltmap[jolts].unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    struct Case {
        input: Vec<Jolt>,
        expected_a: (usize, usize),
        expected_b: BranchCount,
    }

    fn get_test_input() -> Vec<Case> {
        vec![
            Case {
                input: vec![16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4],
                expected_a: (7, 5),
                expected_b: 8,
            },
            Case {
                input: vec![
                    28, 33, 18, 42, 31, 14, 46, 20, 48, 47, 24, 23, 49, 45, 19, 38, 39, 11, 1, 32,
                    25, 35, 8, 17, 7, 9, 4, 2, 34, 10, 3,
                ],
                expected_a: (22, 10),
                expected_b: 19208,
            },
        ]
    }

    #[test]
    fn test_2020_10_a() {
        for i in get_test_input().iter_mut() {
            let mut input: JoltSet = JoltSet::new();
            for j in i.input.iter() {
                input.insert(*j);
            }
            assert_eq!(answer1(&input), i.expected_a);
        }
    }

    #[test]
    fn test_2020_10_b() {
        for i in get_test_input().iter_mut() {
            let mut input: JoltSet = JoltSet::new();
            for j in i.input.iter() {
                input.insert(*j);
            }
            assert_eq!(answer2(&input), i.expected_b);
        }
    }
}
