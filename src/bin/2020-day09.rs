use std::fs::read;

fn main() {
    let input = read_input("input/2020-day09.input");

    let first_non_matching = find_first_non_matching(&input, 25);
    println!("First non-matching value: {}", first_non_matching);

    let (first, last) = find_target_term_range(&input, first_non_matching);

    let slice = &input[first..last + 1];
    let (min, max) = (*slice.iter().min().unwrap(), *slice.iter().max().unwrap());
    println!(
        "Sum of smallest and largest element of term range: {}",
        min + max
    );
}

fn read_input(path: &str) -> Vec<i64> {
    String::from_utf8_lossy(&read(path).expect(&format!("Failed to read '{}'", path)))
        .split_whitespace()
        .filter_map(|s| s.parse().ok())
        .collect()
}

fn find_first_non_matching(input: &Vec<i64>, window_size: usize) -> i64 {
    let mut target_i = window_size;

    for window in input.windows(window_size) {
        if !window
            .iter()
            .any(|i| window.contains(&(input[target_i] - i)))
        {
            return input[target_i];
        }
        target_i += 1;
    }

    panic!("No invalid numbers found");
}

fn find_target_term_range(input: &Vec<i64>, target: i64) -> (usize, usize) {
    for cursor in 0..input.len() {
        let mut acc = input[cursor];

        for i in cursor + 1..input.len() {
            acc += input[i];

            if acc == target {
                return (cursor, i);
            } else if acc > target {
                break;
            }
        }
    }

    panic!("No target sum found");
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str =
        "35 20 15 25 47 40 62 55 65 95 102 117 150 182 127 219 299 277 309 576";

    #[test]
    fn test_1() {
        let input: Vec<i64> = TEST_INPUT
            .split_whitespace()
            .filter_map(|s| s.parse().ok())
            .collect();
        assert_eq!(find_first_non_matching(&input, 5), 127);
    }

    #[test]
    fn test_2() {
        let input: Vec<i64> = TEST_INPUT
            .split_whitespace()
            .filter_map(|s| s.parse().ok())
            .collect();
        let (first, last) = find_target_term_range(&input, 127);
        assert_eq!((first, last), (2, 5));

        let slice = &input[first..last + 1];
        assert_eq!(
            (*slice.iter().min().unwrap(), *slice.iter().max().unwrap()),
            (15, 47)
        );
    }
}
