use std::fmt;

#[derive(Debug, Clone, PartialEq)]
enum Cell {
    Wall,
    Floor,
    Empty,
    Occupied,
}

impl fmt::Display for Cell {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use Cell::*;
        write!(
            f,
            "{}",
            match self {
                Wall => ':',
                Floor => '.',
                Empty => 'L',
                Occupied => '#',
            }
        )
    }
}

impl Cell {
    fn from_char(c: char) -> Self {
        use Cell::*;
        match c {
            ':' => Wall,
            '.' => Floor,
            'L' => Empty,
            '#' => Occupied,
            _ => panic!("Invalid char."),
        }
    }
}

#[derive(Debug, Clone)]
struct Area {
    cells: Vec<Cell>,
    width: usize,
    state_changed: bool,
    steps: usize,
}

impl fmt::Display for Area {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        Ok(for (n, cell) in self.cells.iter().enumerate() {
            write!(f, "{} ", cell).unwrap();
            if (n + 1) % self.width == 0 {
                write!(f, "\n").unwrap()
            };
        })
    }
}

impl Area {
    /// Take the input string, create put wall chars around it, and then transform the whole thing
    /// to internal representation.
    fn from_str(input: &str) -> Self {
        let width = input.find('\n').expect("Malformed input") + 2;
        let mut area = String::new();

        for _ in 0..width {
            area.push(':');
        }

        for line in input.lines() {
            area.push(':');
            area.push_str(line);
            area.push(':');
        }

        for _ in 0..width {
            area.push(':');
        }

        Area {
            cells: area.chars().map(|c| Cell::from_char(c)).collect(),
            width,
            state_changed: false,
            steps: 0,
        }
    }

    /// Perform one step of what I like to call "Conway's Game of Musical Chairs".
    /// The rules can be changed by tolerance and the count function.
    /// If the state doesn't change, the step doesn't count.
    fn step(&mut self, tolerance: usize, count: &dyn Fn(&Vec<Cell>, usize, usize) -> usize) {
        let mut new_cells = Vec::<Cell>::new();

        self.state_changed = false;
        for (n, cell) in self.cells.iter().enumerate() {
            use Cell::*;
            new_cells.push(match cell {
                Wall => Wall,
                Floor => Floor,
                Empty => {
                    if count(&self.cells, self.width, n) == 0 {
                        self.state_changed = true;
                        Occupied
                    } else {
                        Empty
                    }
                }
                Occupied => {
                    if count(&self.cells, self.width, n) >= tolerance {
                        self.state_changed = true;
                        Empty
                    } else {
                        Occupied
                    }
                }
            });
        }

        if self.state_changed {
            self.cells = new_cells;
            self.steps += 1;
        }
    }

    /// Perform an adjecency check by looking at the immediate neighbors.
    fn count_adjacent_occupied(cells: &Vec<Cell>, width: usize, start: usize) -> usize {
        let mut sum = 0;

        use Cell::*;
        if cells[start - width - 1] == Occupied {
            sum += 1;
        }
        if cells[start - width] == Occupied {
            sum += 1;
        }
        if cells[start - width + 1] == Occupied {
            sum += 1;
        }

        if cells[start - 1] == Occupied {
            sum += 1;
        }
        if cells[start + 1] == Occupied {
            sum += 1;
        }

        if cells[start + width - 1] == Occupied {
            sum += 1;
        }
        if cells[start + width] == Occupied {
            sum += 1;
        }
        if cells[start + width + 1] == Occupied {
            sum += 1;
        }

        sum
    }

    /// Perform an adjacency check by looking at neighbors visible in straight lines, blocked by
    /// chairs (empty or occupied). If a wall is hit, there was no chair in the way.
    fn count_visible_occupied(cells: &Vec<Cell>, width: usize, start: usize) -> usize {
        use Cell::*;

        let mut sum = 0;

        let walkers: Vec<Box<dyn Fn(usize) -> usize>> = vec![
            Box::new(|x| -> usize { x - width - 1 }),
            Box::new(|x| -> usize { x - width }),
            Box::new(|x| -> usize { x - width + 1 }),
            Box::new(|x| -> usize { x - 1 }),
            Box::new(|x| -> usize { x + 1 }),
            Box::new(|x| -> usize { x + width - 1 }),
            Box::new(|x| -> usize { x + width }),
            Box::new(|x| -> usize { x + width + 1 }),
        ];

        for walker in walkers {
            let mut i = start;
            loop {
                i = walker(i);
                match cells[i] {
                    Floor => (),
                    Wall | Empty => break,
                    Occupied => {
                        sum += 1;
                        break;
                    }
                }
            }
        }

        sum
    }

    /// Simply count the number of occupied chairs.
    fn count_occupied(&self) -> usize {
        self.cells
            .iter()
            .filter(|&cell| cell == &Cell::Occupied)
            .count()
    }
}

fn main() {
    use std::fs::read;
    let input = String::from_utf8_lossy(&read("input/2020-day11.input").unwrap()).to_string();

    println!("Answer A: {}", answer_a(&input));
    println!("Answer B: {}", answer_b(&input));
}

fn answer_a(input: &str) -> usize {
    let mut area = Area::from_str(&input);

    let tolerance = 4;
    let count_func = &Area::count_adjacent_occupied;

    area.step(tolerance, count_func);
    while area.state_changed {
        area.step(tolerance, count_func);
    }

    area.count_occupied()
}

fn answer_b(input: &str) -> usize {
    let mut area = Area::from_str(&input);

    let tolerance = 5;
    let count_func = &Area::count_visible_occupied;

    area.step(tolerance, count_func);
    while area.state_changed {
        area.step(tolerance, count_func);
    }

    area.count_occupied()
}

#[cfg(test)]
mod tests {
    use super::*;

    fn get_test_input() -> String {
        use std::fs::read;
        String::from_utf8_lossy(&read("test-input/2020-day11-test.input").unwrap()).to_string()
    }

    #[test]
    fn test_2020_11a() {
        let tolerance = 4;
        let count_func = &Area::count_adjacent_occupied;

        let mut area = Area::from_str(&get_test_input());
        assert_eq!(area.state_changed, false);
        assert_eq!(area.steps, 0);

        area.step(tolerance, count_func);
        assert_eq!(area.state_changed, true);
        assert_eq!(area.steps, 1);

        while area.state_changed {
            area.step(tolerance, count_func);
        }
        assert_eq!(area.steps, 5);
        assert_eq!(area.count_occupied(), 37);

        println!("{}", area);
    }

    #[test]
    fn test_2020_11b() {
        let tolerance = 5;
        let count_func = &Area::count_visible_occupied;

        let mut area = Area::from_str(&get_test_input());
        assert_eq!(area.state_changed, false);
        assert_eq!(area.steps, 0);

        area.step(tolerance, count_func);
        assert_eq!(area.state_changed, true);
        assert_eq!(area.steps, 1);

        while area.state_changed {
            println!("{}", area);
            area.step(tolerance, count_func);
        }

        assert_eq!(area.steps, 6);
        assert_eq!(area.count_occupied(), 26);
    }
}
