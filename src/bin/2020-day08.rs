use std::fs::read;

const INPUT_PATH: &str = "input/2020-day08.input";

#[derive(Debug, PartialEq)]
enum Termination {
    Natural(i16),
    Infinite(i16),
    OutOfBounds,
}

#[derive(Debug, Clone)]
enum Opcode {
    Acc,
    Jmp,
    Nop,
}

#[derive(Debug, Clone)]
struct Operation {
    opcode: Opcode,
    value: i16,
    visited: bool,
}

impl Operation {
    fn from(line: &str) -> Self {
        let mut split = line.split_whitespace();
        let (opcode, value): (&str, &str) = (split.next().unwrap(), split.next().unwrap());
        let value: i16 = value.parse().expect(&format!("Invalid value: '{}'", value));

        use Opcode::*;
        Operation {
            opcode: match &opcode[..] {
                "acc" => Acc,
                "jmp" => Jmp,
                "nop" => Nop,
                _ => panic!("Invalid opcode: {}", opcode),
            },
            value,
            visited: false,
        }
    }
}

fn main() {
    let mut program = read_program(INPUT_PATH);
    println!("Answer 1: {:?}", run_program(&mut program));

    let mut bi = 0;
    let program = read_program(INPUT_PATH);
    loop {
        let mut program = program.clone();

        use Opcode::*;
        match program[bi].opcode {
            Jmp => program[bi].opcode = Nop,
            Nop => program[bi].opcode = Jmp,
            _ => (),
        }
        bi += 1;

        match run_program(&mut program) {
            Termination::Natural(value) => {
                println!("Answer 2: {}", value);
                break;
            }
            _ => (),
        }
    }
}

fn read_program(path: &str) -> Vec<Operation> {
    let mut program = Vec::<Operation>::new();

    for line in String::from_utf8_lossy(&read(path).expect(&format!("Error reading '{}'", path)))
        .split('\n')
    {
        if line.is_empty() {
            continue;
        }

        program.push(Operation::from(line));
    }

    program
}

fn run_program(program: &mut Vec<Operation>) -> Termination {
    let mut acc: i16 = 0;
    let mut ip: isize = 0;

    loop {
        if ip == program.len() as isize {
            return Termination::Natural(acc);
        } else if ip > program.len() as isize {
            return Termination::OutOfBounds;
        }

        let operation = &mut program[ip as usize];
        if operation.visited {
            return Termination::Infinite(acc);
        }
        operation.visited = true;

        use Opcode::*;
        match operation.opcode {
            Acc => {
                acc += operation.value;
                ip += 1;
            }
            Jmp => {
                ip += operation.value as isize;
            }
            Nop => {
                ip += 1;
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_run_program() {
        let mut program = read_program("test-input/2020-day08-test.input");
        assert_eq!(run_program(&mut program), Termination::Infinite(5));
    }

    #[test]
    fn test_modify_program() {
        let mut program = read_program("test-input/2020-day08-test.input");
        program[7].opcode = Opcode::Nop;
        assert_eq!(run_program(&mut program), Termination::Natural(8));
    }
}
