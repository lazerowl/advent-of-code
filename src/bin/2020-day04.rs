use std::{error::Error, fs::read, str};

#[derive(Default, Debug)]
struct Passport {
    byr: Option<String>,
    iyr: Option<String>,
    eyr: Option<String>,
    hgt: Option<String>,
    hcl: Option<String>,
    ecl: Option<String>,
    pid: Option<String>,
    cid: Option<String>,
    has_required_fields: bool,
    fields_are_valid: bool,
}

impl Passport {
    fn from_string(s: &str) -> Self {
        let mut passport = Self::default();

        for entry in s.split_whitespace() {
            let mut it = entry.split(':');
            let (key, value): (&str, &str) = (it.next().unwrap(), it.next().unwrap());
            match key {
                "byr" => passport.byr = Some(value.to_string()),
                "iyr" => passport.iyr = Some(value.to_string()),
                "eyr" => passport.eyr = Some(value.to_string()),
                "hgt" => passport.hgt = Some(value.to_string()),
                "hcl" => passport.hcl = Some(value.to_string()),
                "ecl" => passport.ecl = Some(value.to_string()),
                "pid" => passport.pid = Some(value.to_string()),
                "cid" => passport.cid = Some(value.to_string()),
                _ => (),
            }
        }

        passport.has_required_fields = [
            &passport.byr,
            &passport.iyr,
            &passport.eyr,
            &passport.hgt,
            &passport.hcl,
            &passport.ecl,
            &passport.pid,
        ]
        .iter()
        .all(|x| x.is_some());

        passport.fields_are_valid = passport.validate_fields();

        passport
    }

    fn validate_fields(&self) -> bool {
        if !self.has_required_fields {
            return false;
        }

        if !Self::check_year_bound(&self.byr, 1920, 2002) {
            return false;
        }
        if !Self::check_year_bound(&self.iyr, 2010, 2020) {
            return false;
        }
        if !Self::check_year_bound(&self.eyr, 2020, 2030) {
            return false;
        }
        if !Self::check_height_bound(&self.hgt) {
            return false;
        }
        if !Self::check_hair_color(&self.hcl) {
            return false;
        }
        if !Self::check_eye_color(&self.ecl) {
            return false;
        }
        if !Self::check_pid(&self.pid) {
            return false;
        }

        return true;
    }

    fn check_year_bound(field: &Option<String>, min: u16, max: u16) -> bool {
        if field.is_none() {
            return false;
        }

        let year: u16 = match field.as_ref().unwrap().parse() {
            Ok(value) => value,
            Err(_) => return false,
        };

        return min <= year && year <= max;
    }

    fn check_height_bound(value: &Option<String>) -> bool {
        if value.is_none() {
            return false;
        }
        let value: &str = &value.as_ref().unwrap();

        let unit: String = value.chars().rev().take(2).collect();

        match &unit[..] {
            "ni" => match value.chars().take(2).collect::<String>().parse() {
                Ok(value) => 59 <= value && value <= 76,
                Err(_) => false,
            },
            "mc" => match value.chars().take(3).collect::<String>().parse() {
                Ok(value) => 150 <= value && value <= 193,
                Err(_) => false,
            },
            _ => false,
        }
    }

    fn check_hair_color(value: &Option<String>) -> bool {
        if value.is_none() {
            return false;
        }
        let value: &str = &value.as_ref().unwrap();

        if value.len() != 7 {
            return false;
        }

        match value.chars().nth(0) {
            Some(value) => {
                if value != '#' {
                    return false;
                }
            }
            None => return false,
        }

        value[1..7].chars().all(|x| x.is_ascii_hexdigit())
    }

    fn check_eye_color(value: &Option<String>) -> bool {
        if value.is_none() {
            return false;
        }
        let value: &str = &value.as_ref().unwrap();

        ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]
            .iter()
            .any(|&x| x == value)
    }

    fn check_pid(value: &Option<String>) -> bool {
        if value.is_none() {
            return false;
        }
        let value: &str = &value.as_ref().unwrap();

        if value.len() != 9 {
            return false;
        };

        value.chars().all(|x| x.is_ascii_digit())
    }

    fn from_batch(path: &str) -> Result<Vec<Self>, Box<dyn Error>> {
        Ok(str::from_utf8(&read(path)?)?
            .split("\n\n")
            .map(|x| Passport::from_string(x))
            .collect())
    }
}

fn main() {
    match answer1("input/2020-day04.input") {
        Ok(answer) => println!("Answer 1: {}", answer),
        Err(error) => println!("Failed to run answer 1: {}", error),
    };

    match answer2("input/2020-day04.input") {
        Ok(answer) => println!("Answer 2: {}", answer),
        Err(error) => println!("Failed to run answer 2: {}", error),
    };
}

fn answer1(path: &str) -> Result<usize, Box<dyn Error>> {
    Ok(Passport::from_batch(path)?
        .iter()
        .filter(|x| x.has_required_fields)
        .count())
}

fn answer2(path: &str) -> Result<usize, Box<dyn Error>> {
    Ok(Passport::from_batch(path)?
        .iter()
        .filter(|x| x.fields_are_valid)
        .count())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_check_year_bound() {
        assert_eq!(false, Passport::check_year_bound(&None, 0, 0));
        assert_eq!(
            false,
            Passport::check_year_bound(&Some("not an integer".to_string()), 0, 0)
        );
        assert_eq!(
            false,
            Passport::check_year_bound(&Some("2000".to_string()), 2001, 2002)
        );
        assert_eq!(
            false,
            Passport::check_year_bound(&Some("2000".to_string()), 1998, 1999)
        );
        assert_eq!(
            true,
            Passport::check_year_bound(&Some("2000".to_string()), 1999, 2000)
        );
        assert_eq!(
            true,
            Passport::check_year_bound(&Some("2000".to_string()), 2000, 2001)
        );
    }

    #[test]
    fn test_check_height_bound() {
        assert_eq!(false, Passport::check_height_bound(&None));
        assert_eq!(
            false,
            Passport::check_height_bound(&Some("not a valid string".to_string()))
        );
        assert_eq!(
            false,
            Passport::check_height_bound(&Some("190".to_string()))
        );
        assert_eq!(
            false,
            Passport::check_height_bound(&Some("190in".to_string()))
        );
        assert_eq!(
            true,
            Passport::check_height_bound(&Some("60in".to_string()))
        );
        assert_eq!(
            true,
            Passport::check_height_bound(&Some("190cm".to_string()))
        );
    }

    #[test]
    fn test_check_hair_color() {
        assert_eq!(false, Passport::check_hair_color(&None));
        assert_eq!(
            false,
            Passport::check_hair_color(&Some("not a valid string".to_string()))
        );
        assert_eq!(
            false,
            Passport::check_hair_color(&Some("abc123".to_string()))
        );
        assert_eq!(
            false,
            Passport::check_hair_color(&Some("#abz123".to_string()))
        );
        assert_eq!(
            true,
            Passport::check_hair_color(&Some("#abc123".to_string()))
        );
    }

    #[test]
    fn test_check_eye_color() {
        assert_eq!(false, Passport::check_eye_color(&None));
        assert_eq!(false, Passport::check_eye_color(&Some("abc".to_string())));
        assert_eq!(true, Passport::check_eye_color(&Some("amb".to_string())));
        assert_eq!(true, Passport::check_eye_color(&Some("brn".to_string())));
    }

    #[test]
    fn test_check_pid() {
        assert_eq!(false, Passport::check_pid(&None));
        assert_eq!(false, Passport::check_pid(&Some("abc".to_string())));
        assert_eq!(false, Passport::check_pid(&Some("0123456789".to_string())));
        assert_eq!(true, Passport::check_pid(&Some("000000001".to_string())));
    }
}
