static INPUT: &str = "1000053
19,x,x,x,x,x,x,x,x,x,x,x,x,37,x,x,x,x,x,523,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,13,x,x,x,x,23,x,x,x,x,x,29,x,547,x,x,x,x,x,x,x,x,x,41,x,x,x,x,x,x,17";

fn main() {
    let mut lines = INPUT.lines();
    let arrival: usize = lines.next().unwrap().parse().unwrap();
    let departures: Vec<usize> = lines
        .next()
        .unwrap()
        .split(',')
        .filter_map(|s| s.parse().ok())
        .collect();

    println!("Answer A: {}", answer_a(arrival, &departures));

    let mut lines = INPUT.lines();
    let departures = lines
        .nth(1)
        .unwrap()
        .split(',')
        .map(|s| match s.parse::<usize>() {
            Ok(value) => value,
            Err(_) => 1,
        })
        .collect();
    println!("Answer B: {}", answer_b(&departures));
}

fn answer_a(arrival: usize, departures: &Vec<usize>) -> usize {
    let (wait_time, id) = departures
        .iter()
        .map(|id| ((arrival - (arrival % id) + id) - arrival, id))
        .min()
        .unwrap();
    wait_time * id
}

fn answer_b(departures: &Vec<usize>) -> usize {
    let coprime: usize = departures.iter().product();

    let mut factors: Vec<usize> = Vec::new();

    for (i, id) in departures.iter().rev().enumerate() {
        let factor = coprime / id;
        let mut j = 1;
        while (factor * j) % id != i % id {
            j += 1;
        }
        factors.push(factor * j);
    }

    eprintln!("{:?}", factors);

    factors.iter().sum::<usize>() % coprime - departures.len() + 1
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_2020_13a() {
        let arrival = 939;
        let departures: Vec<usize> = "7,13,x,x,59,x,31,19"
            .split(',')
            .filter_map(|s| s.parse::<usize>().ok())
            .collect();

        assert_eq!(answer_a(arrival, &departures), 295);
    }

    #[test]
    fn test_2020_13b1() {
        let departures: Vec<usize> = "7,13,x,x,59,x,31,19"
            .split(',')
            .map(|s| match s.parse::<usize>() {
                Ok(value) => value,
                Err(_) => 1,
            })
            .collect();

        assert_eq!(answer_b(&departures), 1068781);
    }

    #[test]
    fn test_2020_13b2() {
        let departures: Vec<usize> = "17,x,13,19"
            .split(',')
            .map(|s| match s.parse::<usize>() {
                Ok(value) => value,
                Err(_) => 1,
            })
            .collect();

        assert_eq!(answer_b(&departures), 3417);
    }
}
