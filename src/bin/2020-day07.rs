use std::{collections::HashMap, fs::read};

fn main() {
    let bags = build_bagmap("input/2020-day07.input");

    // Check all colors for shiny gold bags somewhere in the search tree
    let mut count = 0;
    for (key_color, _) in &bags {
        if search_color(&bags, key_color, &"shiny gold".to_string()) {
            count += 1
        }
    }

    println!("Answer 1: {}", count);
    println!(
        "Answer 2: {}",
        count_contents(&bags, &"shiny gold".to_string()) - 1
    );
}

fn build_bagmap(path: &str) -> HashMap<String, Vec<(usize, String)>> {
    let mut bags = HashMap::<String, Vec<(usize, String)>>::new();

    let buffer = read(path).expect(&format!("Failed to read '{}'", path));
    let input = String::from_utf8_lossy(&buffer);
    let lines = &input.split('\n').collect::<Vec<&str>>();

    for line in lines {
        if line.is_empty() {
            break;
        }

        let mut tokens = line.split_whitespace();

        // Subject bag
        let key_color = &format!("{} {}", tokens.next().unwrap(), tokens.next().unwrap());

        bags.insert(key_color.to_string(), Vec::<(usize, String)>::new());

        // Delimiter (literal "bags contain")
        tokens.next().unwrap();
        tokens.next().unwrap();

        // List of contained bags
        loop {
            let count = tokens.next().unwrap();
            if count == "no" {
                break;
            }
            let count = count.parse().unwrap();

            let content_color = format!("{} {}", tokens.next().unwrap(), tokens.next().unwrap());

            bags.get_mut(key_color)
                .unwrap()
                .push((count, content_color));

            // Terminator is either "bags," or "bag."
            let terminator = tokens.next().unwrap();
            if terminator.ends_with('.') {
                break;
            }
        }
    }

    bags
}

fn search_color(
    bags: &HashMap<String, Vec<(usize, String)>>,
    start_color: &String,
    needle: &String,
) -> bool {
    let contents = bags.get(start_color).unwrap();

    if contents.is_empty() {
        return false;
    }

    for (_count, color) in contents {
        if color == needle || search_color(bags, color, needle) {
            return true;
        }
    }

    false
}

fn count_contents(bags: &HashMap<String, Vec<(usize, String)>>, start_color: &String) -> usize {
    let contents = bags.get(start_color).unwrap();

    if contents.is_empty() {
        return 1;
    }

    let mut sum = 1;
    for (count, color) in contents {
        let content_sum = count_contents(bags, color);
        sum += count * content_sum;
    }

    sum
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_count_contents() {
        let bags = build_bagmap("test-input/2020-day07-test.input");
        assert_eq!(32, count_contents(&bags, &"shiny gold".to_string()) - 1);
    }
}
